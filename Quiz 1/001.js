// A. Balik String
console.log("============== A. Balik String ==============");
balikString = (kata) => {
    let temp = ''
    for (let i = kata.length - 1; i >= 0; i--) {
        temp += kata[i]
    }
    return temp
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah


// B. Palindrome
console.log("============== B. Palindrome ==============");
palindrome = (kata) => {
    let temp = ''
    for (let i = kata.length - 1; i >= 0; i--) {
        temp += kata[i]
    }
    
    if (kata === temp) {
        return true
    } else {
        return false
    }
}

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

// C. Bandingkan Angka
console.log("============== C. Bandingkan Angka ==============");
bandingkan = (numb1, numb2) => {
    if (numb1 > 0 && numb2 > 0) {
        if (numb1 === numb2) {
            return -1
        } else {
            return Math.max(numb1, numb2)
        }
    } else {
        return -1
    }
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18