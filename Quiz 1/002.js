// A. Ascending Ten
console.log("============== A. Ascending Ten ==============");
AscendingTen = (number) => {
    let temp = ''
    if (number) {
        for (let i = number; i < (number + 10); i++) {
            temp += `${i} `
        }
    } else {
        temp = '-1'
    }
    return temp
}

console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1


// B. Descending Ten
console.log("============== B. Descending Ten ==============");
DescendingTen = (number) => {
    let temp = ''
    if (number) {
        for (let i = number; i > (number - 10); i--) {
            temp += `${i} `
        }
    } else {
        temp = '-1'
    }
    return temp
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1


// C. Conditional Ascending Descending
console.log("============== C. Conditional Ascending Descending ==============");
ConditionalAscDesc = (reference, check) => {
    let temp = ''
    if (reference && check) {
        if (check % 2 !== 0) {
            for (let i = reference; i < (reference + 10); i++) {
                temp += `${i} `
            }
        } else {
            for (let i = reference; i > (reference - 10); i--) {
                temp += `${i} `
            }
        }
    } else {
        temp = '-1'
    }
    return temp
}

console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1


// D. Papan Ular Tangga
console.log("============== D. Papan Ular Tangga ==============");
ularTangga = () => {
    let board = ''
    for (let i = 10; i >= 1; i--) {
        let temp = ''
        if (i % 2 === 0) {
            for (let j = (i * 10); j >= ((i * 10) - 9); j--) {
                temp += `${j} `
            }
        } else {
            for (let j = (i * 10) - 9; j <= (i * 10); j++) {
                temp += `${j} `
            }
        }
        board += `${temp} \n`
    }
    return board
}

console.log(ularTangga());