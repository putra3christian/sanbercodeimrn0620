/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
    // Code disini
    constructor(subject, points, email) {
        this._subject = subject
        this._points = points
        this._email = email
    }

    get averagePoints() {
        let number

        if (typeof this._points === "number") {
            number = this._points
        } else if (Array.isArray(this._points)) {
            number = this._points.reduce((a, b) => a + b, 0) / this._points.length
        } else {
            number = "Points bukan integer!"
        }

        return number
    }
}

const score = new Score('subject', [10, 15, 13], 'email')
console.log(score.averagePoints)


/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

viewScores = (data, subject) => {
    // code kamu di sini
    let output = []

    for (let i = 1; i < data.length; i++) {
        output.push(
            {
                email: data[i][0],
                subject,
                points: data[i][data[0].indexOf(subject)]
            }
        )
    }

    console.log(output);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

recapScores = (data) => {
    // code kamu di sini
    for (let i = 1; i < data.length; i++) {
        let average = data[i].slice(1).reduce((a, b) => a + b, 0) / (data[i].length - 1)
        let output = `
            ${i}. Email: ${data[i][0]}
            Rata-rata: ${Math.ceil(average * 10) / 10}
            Predikat: ${average > 90 ? "honour" : average > 80 ? "graduate" : average > 70 ? "participant" : ""}
        `
        console.log(output);
    }
}

recapScores(data);