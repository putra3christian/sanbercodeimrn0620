import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

export default class Navbar extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.navWrapper}>
          <MaterialIcons name="home" style={styles.navigation} size={35} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.navWrapper}>
          <MaterialIcons name="person" style={styles.navigation} size={35} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.navWrapper}>
          <MaterialIcons name="help" style={styles.navigation} size={35} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    height: 75,
    borderWidth: 5,
    borderColor: "#388BF2",
    borderRadius: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20,
    marginHorizontal: 30,
},
navWrapper: {
    backgroundColor: "#31B057",
    borderRadius: 10
},
  navigation: {
      margin: 5,
      color: "#ffffff"
  },
});
