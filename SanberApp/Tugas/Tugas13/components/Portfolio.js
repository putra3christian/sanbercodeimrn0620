import React, { Component } from "react";
import { View, StyleSheet, FlatList, Image, Text } from "react-native";

export default class Portfolio extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{marginBottom: 10}}>Portfolio</Text>
        <FlatList
          numColumns={2}
          style={styles.list}
          data={new Array(50).fill(0)}
          renderItem={() => (
            <Image
              source={{
                uri: "https://picsum.photos/500",
              }}
              style={{ width: 150, height: 150, margin: 10 }}
            />
          )}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  list: {
    marginBottom: 10,
  },
});
