import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Ionicons style={styles.logo} name="md-cube" size={100} />
          <Text style={styles.title}>Putra's App</Text>
        </View>
        <View style={styles.body}>
          <Text style={{ fontSize: 35, fontWeight: "400" }}>Login</Text>
          <TextInput
            style={styles.input}
            placeholder="email"
            autoCompleteType="email"
          />
          <TextInput
            style={styles.input}
            placeholder="password"
            secureTextEntry={true}
          />
        </View>
        <TouchableOpacity style={styles.login} onPress={() => this.props.navigation.navigate('Profile')}>
          <Text style={{ color: "#ffffff", fontSize: 20 }}>Sign in</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  header: {
    flex: 0.5,
    width: 375,
    backgroundColor: "#31B057",
    justifyContent: "center",
    alignItems: "center",
  },
  logo: {
    color: "#ffffff",
  },
  title: {
    fontSize: 30,
    color: "#ffffff",
  },
  body: {
    paddingTop: 60,
    alignItems: "center",
  },
  input: {
    height: 40,
    width: 250,
    borderColor: "black",
    borderBottomWidth: 1,
  },
  login: {
    width: 150,
    height: 50,
    backgroundColor: "#388BF2",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 35,
  },
});
