import React, { Component } from "react";
import { View, StyleSheet, ImageBackground, Image, Text } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Navbar from "../components/Navbar";
import Portfolio from "../components/Portfolio";

export default class Profile extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <ImageBackground
            style={styles.background}
            source={{ uri: "https://picsum.photos/500" }}
          />
          <View style={styles.profile}>
            <Image
              source={{
                uri: `https://randomuser.me/api/portraits/men/${Math.floor(
                  Math.random() * 10
                )}.jpg`
              }}
              style={{
                width: 150,
                height: 150,
                borderRadius: 100,
                borderWidth: 5,
              }}
            />
            <Text style={{ fontSize: 30, marginTop: 20 }}>John Doe</Text>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.social}>
            <Text>Find me!</Text>
            <View style={styles.logo}>
              <Ionicons name="logo-facebook" size={45} />
              <Ionicons name="logo-twitter" size={45} />
              <Ionicons name="logo-instagram" size={45} />
              <Ionicons name="logo-linkedin" size={45} />
            </View>
          </View>
          <Portfolio />
        </View>
        <Navbar />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 0.4,
  },
  background: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    alignItems: "center",
  },
  profile: {
    alignItems: "center",
    marginTop: -75,
  },
  body: {
    flex: 0.6,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  social: {
    flex: 0.3,
    alignItems: "center",
  },
  logo: {
    width: 275,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
});
