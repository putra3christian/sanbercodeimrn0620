import React, { Component } from 'react'
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet, FlatList, Image } from 'react-native';
import Navbar from '../Tugas13/components/Navbar';
import skill from './skillData.json'

export default class SkillScreen extends Component {
    listSkill = (category) => {
        let list = skill.items.map((data, key) =>
            <View key={key}>
                {data.category === category ?
                    <>
                        <Image source={{ uri: data.logoUrl }} style={{ width: 50, height: 50 }} />
                        <Text>
                            {data.skillName}
                        </Text>
                        <Text>{data.percentageProgress}</Text>
                    </>
                    :
                    <></>
                }
            </View>
        )
        return list
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.username}>Putra Christian</Text>
                </View>
                <View style={{flex: 1, alignItems:'center', justifyContent: 'flex-end'}}>
                    <ScrollView contentContainerStyle={styles.skill}>
                        <Text>Language</Text>
                        <ScrollView horizontal>
                            {this.listSkill('Language')}
                        </ScrollView>
                        <Text>Library / Framework</Text>
                        <ScrollView horizontal>
                            {this.listSkill('Library')}
                        </ScrollView>
                        <Text>Technology</Text>
                        <ScrollView horizontal>
                            {this.listSkill('Technology')}
                        </ScrollView>
                    </ScrollView>
                </View>

                <Navbar />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        height: '30%',
        backgroundColor: '#31B057',
        borderBottomWidth: 6,
        borderBottomColor: '#388BF2',
        flexDirection: 'column',
        justifyContent: "flex-end",
        paddingBottom: '7%',
        paddingLeft: '5%'
    },
    username: {
        color: '#ffffff',
        fontSize: 45,
        width: '50%'
    },
    skill: {
        flex: 1,
    },
    contentContainer: {
        alignItems: 'center',
        width: '100%',
    }
})