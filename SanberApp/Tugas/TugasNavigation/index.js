import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import SkillScreen from '../Tugas14/SkillScreen'
import Profile from '../Tugas13/screens/Profile'
import Login from '../Tugas13/screens/Login'
import AddScreen from './AddScreen'
import ProjectScreen from './ProjectScreen'

const AuthStack = createStackNavigator()
const Drawer = createDrawerNavigator()
const Tabs = createBottomTabNavigator()

const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillScreen} />
        <Tabs.Screen name="Project" component={ProjectScreen} />
        <Tabs.Screen name="Add" component={AddScreen} />
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="Login" component={Login} />
        <Drawer.Screen name="Tab" component={TabsScreen} />
        <Drawer.Screen name="Profile" component={Profile} />
    </Drawer.Navigator>
)

export default () => (
    <NavigationContainer>
        <AuthStack.Navigator>
            <AuthStack.Screen name="Login" component={DrawerScreen} />
        </AuthStack.Navigator>
    </NavigationContainer>
)