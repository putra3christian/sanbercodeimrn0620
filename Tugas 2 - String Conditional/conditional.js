// IF-ELSE
var nama = "John"
var peran = "Penyihir"

if (nama === "") {
    console.log("Nama harus diisi!");
} else if (peran === "") {
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
} else {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);

    if (peran === "Penyihir") {
        console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
    } else if (peran === "Guard") {
        console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
    } else if (peran === "Werewolf") {
        console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`);
    }
}


// Switch Case
var hari = 21;
var bulan = 1;
var tahun = 1945;

switch (bulan) {
    case 1:
        bulan = "Januari"
        break;
    case 2:
        bulan = "Februari"
        break;
    case 3:
        bulan = "April"
        break;
    case 4:
        bulan = "Maret"
        break;
    case 5:
        bulan = "Mei"
        break;
    case 6:
        bulan = "Juni"
        break;
    case 7:
        bulan = "Juli"
        break;
    case 8:
        bulan = "Agustus"
        break;
    case 9:
        bulan = "September"
        break;
    case 10:
        bulan = "Oktober"
        break;
    case 11:
        bulan = "Nopember"
        break;
    case 12:
        bulan = "Desember"
        break;
    default:
        break;
}

console.log(`${hari} ${bulan} ${tahun}`);
