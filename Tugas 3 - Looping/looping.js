// No. 1 Looping While
loopingWhile()
function loopingWhile() {
    i = 0

    console.log("LOOPING PERTAMA");
    while (i < 20) {
        i = i + 2
        console.log(`${i} - I love coding`);
    }

    console.log("LOOPING KEDUA");
    while (i > 0) {
        console.log(`${i} - I will become a mobile developer`);
        i = i - 2
    }
}

// No. 2 Looping menggunakan for
loopingFor()
function loopingFor() {
    for (let i = 1; i <= 20; i++) {
        if (i % 2 === 0) {
            console.log(`${i} - Berkualitas`);
        } else {
            if (i % 3 === 0) {
                console.log(`${i} - I Love Coding`);
            } else {
                console.log(`${i} - Santai`);
            }
        }
    }
}

// No. 3 Membuat Persegi Panjang #
persegiPanjang()
function persegiPanjang() {
    for (let i = 0; i < 4; i++) {
        temp = ""
        for (let j = 0; j < 8; j++) {
            temp += '#'
        }
        console.log(temp);
    }
}

// No. 4 Membuat Tangga
tangga()
function tangga() {
    temp = ""
    for (let i = 0; i < 7; i++) {
        temp += "#"
        console.log(temp);
    }
}

// No. 5 Membuat Papan Catur
papanCatur()
function papanCatur() {
    for (let i = 0; i < 8; i++) {
        temp = ""
        for (let j = 0; j < 4; j++) {
            if (i % 2 !== 0) {
                temp += "# "
            } else {
                temp += " #"
            }
        }
        console.log(temp);
    }
}