// Soal No. 1 (Range)
range = (startNum, finishNum) => {
    const newArray = []
    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i++) {
            newArray.push(i)
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i--) {
            newArray.push(i)
        }
    } else {
        newArray.push(-1)
    }
    return newArray
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Soal No. 2 (Range with Step)
rangeWithStep = (startNum, finishNum, step) => {
    const newArray = []
    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            newArray.push(i)
        }
    } else {
        for (let i = startNum; i >= finishNum; i -= step) {
            newArray.push(i)
        }
    }
    return newArray
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


// Soal No. 3 (Sum of Range)
sum = (startNum, finishNum, step) => {
    const newArray = []
    step ? step : step = 1
    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            newArray.push(i)
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            newArray.push(i)
        }
    } else {
        newArray.push(startNum ? startNum : 0)
    }
    return newArray.reduce((a, b) => a + b, 0)
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


// Soal No. 4 (Array Multidimensi)
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling = (data) => {
    let temp = ''
    data.forEach(e => {
        temp += `
            Nomor ID: ${e[0]}
            Nama Lengkap: ${e[1]}
            TTL: ${e[2]} ${e[3]}
            Hobi: ${e[4]}
        `
    })
    return temp
}

console.log(dataHandling(input));


// Soal No. 5 (Balik Kata)
balikKata = (kata) => {
    let temp = ''
    for (let i = kata.length - 1; i >= 0; i--) {
        temp += kata[i]
    }
    return temp
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


// Soal No. 6 (Metode Array)
dataHandling2 = (input) => {
    // output
    let output = input
    output[1] = output[1] + 'Elsharawy'
    output[2] = 'Provinsi' + output[2]
    output.splice(4, 0, 'Pria')
    output.splice(5, 0, 'SMA Internasional Metro')
    output.splice(6, 1)
    console.log(output);

    // month
    let month = output[3].split('/')
    switch (month[1]) {
        case '01':
            month = 'Januari'
            break;
        case '02':
            month = 'Februari'
            break;
        case '03':
            month = 'Maret'
            break;
        case '04':
            month = 'April'
            break;
        case '05':
            month = 'Mei'
            break;
        case '06':
            month = 'Juni'
            break;
        case '07':
            month = 'Juli'
            break;
        case '08':
            month = 'Agustus'
            break;
        case '09':
            month = 'September'
            break;
        case '10':
            month = 'Oktober'
            break;
        case '11':
            month = 'Nopember'
            break;
        case '12':
            month = 'Desember'
            break;
        default:
            break;
    }
    console.log(month);


    // date
    let date = output[3].split('/')
    let joinDate = date.join('-')
    let sortedDate = date.sort((a, b) => b - a)
    console.log(sortedDate);
    console.log(joinDate)


    // slice name
    let name = output[1]
    let spliceName = name.slice(0, 15)
    console.log(spliceName);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);