// Soal No. 1 (Array to Object)
console.log("========== Array to Object ==========");

function arrayToObject(arr) {
    let currentYear = new Date().getFullYear()
    let peopleObj = {}
    arr.forEach((element, i) => {
        peopleObj[`${i + 1}. ${element[0]} ${element[1]}`] = {
            firstName: element[0],
            lastName: element[1],
            gender: element[2],
            age: element[3] < currentYear ? currentYear - element[3] : 'Invalid Birth Year'
        }
    });
    console.log(peopleObj);
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: {
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: {
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""


// Soal No. 2 (Shopping Time)
console.log("========== Shopping Time ==========");
function shoppingTime(memberId, money) {
    let product = [
        { nama: 'Sepatu Stacattu', harga: 1500000 },
        { nama: 'Baju Zoro', harga: 500000 },
        { nama: 'Baju H&N', harga: 250000 },
        { nama: 'Sweater Uniklooh', harga: 175000 },
        { nama: 'Casing Handphone', harga: 50000 },
    ]

    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        let shop = {
            memberId: memberId,
            money: money,
            listPurchased: [],
            changeMoney: money
        }
        product.forEach(element => {
            if (money >= element.harga) {
                shop.listPurchased.push(element.nama)
                shop.changeMoney = shop.changeMoney - element.harga
            }
        })

        return shop
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


// Soal No. 3 (Naik Angkot)
console.log("========== Naik Angkot ==========");
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let total = []

    arrPenumpang.forEach(element => {
        total.push({
            penumpang: element[0],
            naikDari: element[1],
            tujuan: element[2],
            bayar: (rute.indexOf(element[2]) - rute.indexOf(element[1])) * 2000
        })
    })
    return total
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]