// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let i = 0
startRead = (time) => {
    readBooks(time, books[i], sisaWaktu => {
        if (sisaWaktu !== time && i < 3) {
            startRead(sisaWaktu)
        }
        i++
    })
}

startRead(10000)